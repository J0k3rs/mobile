package com.jokers.mobile.detect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Oum {

    private static final Logger logger =
            LoggerFactory.getLogger(Oum.class);

 /*   @RequestMapping("/")
    public String sitePref(SitePreference sitePreference, Model model){
        if (sitePreference == SitePreference.NORMAL){
            return "Home";
        }else if (sitePreference == SitePreference.MOBILE){
            return "Mobile";
        }else {
            return "Home";
        }
    }
*/
    @RequestMapping("/detect")
    public String home(Device device){

        String deviceType = null;
        String platform = null;

        if (device.isMobile()){
            deviceType = "Mobile";
        }else if (device.isTablet()){
            deviceType = "Tablet";
        }else if (device.isNormal()){
            deviceType = "Browser";
        }

        platform = device.getDevicePlatform().name();

        if (platform.equalsIgnoreCase("UNKNOWN")){
            platform = "Browser";
        }
        return deviceType + " device on platform - " + platform;
    }
}
